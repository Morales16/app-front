import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Domicilio } from 'src/app/interfaces/domicilio.interface';
import { Router } from '@angular/router';
import { loadStripe } from '@stripe/stripe-js'
import { PedidoService } from '../../services/pedido.service';

@Component({
  selector: 'app-domicilio',
  templateUrl: './domicilio.page.html',
  styleUrls: ['./domicilio.page.scss'],
})
export class DomicilioPage implements OnInit {

  domicilios: Array<Domicilio> = [];
  stripe;

  constructor(public userService: UserService, private router: Router, public pedidoService: PedidoService) { }

  async ngOnInit() {
    this.stripe = await loadStripe('pk_test_51HHYmeEXs8laua6cIDAYV5UBjmG7hK3DbVHIGBk7SQRKR8hi9MQs74AZh3SfgvEHMmj9HMJq3kyaUenAhRNlJI2300QaB0MOZ8');
  }


  next() {
    this.pedidoService.checkOut(JSON.parse(localStorage.getItem('cart')))
      .subscribe((res: any) => { 
        console.log(res)
        this.stripe.redirectToCheckout({
          sessionId: res.session_id
        })
          .then(function(res) {
          alert('OK')
          console.log(res)
        })
      }, err => console.error(err))
  }
}

