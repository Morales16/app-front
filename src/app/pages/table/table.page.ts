import { Component, OnInit } from '@angular/core';
import { Table } from '../../interfaces/table';
import { TableService } from '../../services/table.service';
import { LoadingController, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-table',
  templateUrl: './table.page.html',
  styleUrls: ['./table.page.scss'],
})
export class TablePage implements OnInit {

  Tables: Array<Table> = [];
  filterTables: Array<Table> = [];
  load;

  constructor(
    public tabService: TableService,
    public loadingCtrl: LoadingController,
    public menuCtrl: MenuController
  ) { }

  async ngOnInit() {
    await this.loading('Por favor espere...');
    this.getTables();
    this.menuCtrl.enable(false)
  }

  getTables() {
    this.tabService.getTables()
    .subscribe((res: Table[]) => {
      this.Tables = res;
      this.filterTables = this.Tables;
      this.load.dismiss();
    }, err => {
      console.log(err)
    })
  }

  async loading(mensaje) {
    this.load = await this.loadingCtrl.create({
      cssClass: 'loading',
      message: mensaje,
    });
    await this.load.present();
    return 'OK';
  }

}
