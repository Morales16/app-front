import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PedidoService } from '../../services/pedido.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

  constructor(
    private _route: ActivatedRoute,
    public pedidoService: PedidoService,
  ) { }

  ngOnInit() {
    this._route.params.subscribe(params => {
      if(params.session) {
        this.pedidoService.checkSession(params.session)
        .subscribe(res => {
            localStorage.removeItem('cart');
        }, err => console.log(err))
      }
    })
  }

}
