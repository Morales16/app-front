import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class TableService {

  apiUrl = 'http://localhost:3000/table';

  constructor(private http: HttpClient, public authService: AuthService) { }

  
  getTables() {
    return this.http.get(`${this.apiUrl}`, {headers: new HttpHeaders({
      'Authorization': `Bearer ${this.authService.getToken()}`
    })});
  }
}
